name := "notkea"

version := "1.0.0"

scalaVersion := "2.11.7"

libraryDependencies ++= {
  val akkaVersion  = "2.3.9"

  def excludeFromAll(items: Seq[ModuleID], group: String, artifact: String) =
    items.map(_.exclude(group, artifact))

  val deps = Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.google.inject" % "guice" % "4.0",
    "com.github.tototoshi" %% "scala-csv" % "1.2.1",
    "org.scalatest" %% "scalatest" % "2.2.1" % "test",
    "com.typesafe.akka" %% "akka-testkit" % akkaVersion % "test"
  )

  excludeFromAll(deps, "com.google.code.findbugs", "jsr305")
}

scalacOptions ++= Seq(
  "-unchecked",
  "-deprecation",
  "-Xlint",
  "-Ywarn-dead-code",
  "-language:_",
  "-target:jvm-1.8",
  "-encoding", "UTF-8"
)
