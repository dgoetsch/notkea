package com.enragedginger.notkea.actors

import javax.inject.Inject

import akka.actor.ActorRef
import com.enragedginger.notkea.actors.Cashier.{CheckoutResult, CheckoutReq}
import com.enragedginger.notkea.actors.StoreManager.RecordSale
import com.enragedginger.notkea.akkaguice.NamedActor
import com.enragedginger.notkea.models.Item
import com.google.inject.name.Named

/**
 * Represents a cashier in a Notkea store.
 * @author Stephen Hopper.
 */
class Cashier @Inject()(@Named("storemanager") storeManager: ActorRef) extends BaseNotkeaActor {
  override def receive: Receive = {
    case req: CheckoutReq =>
      log.info(s"Received checkout request for ${req.items.size} items.")
      //Thread.sleep(new Random().nextInt(5000))
      val totalPrice = req.items.map(_._2.price).sum
      storeManager ! RecordSale(units = req.items.map(_._1).sum, price = totalPrice)
      sender() ! CheckoutResult(totalPrice)
  }
}

object Cashier extends NamedActor {
  override final val name = "cashier"

  case class CheckoutReq(items: Seq[(Int, Item)])
  case class CheckoutResult(totalPrice: Float)
}