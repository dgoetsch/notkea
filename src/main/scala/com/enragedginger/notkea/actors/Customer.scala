package com.enragedginger.notkea.actors

import javax.inject.Inject

import akka.actor.ActorRef
import akka.util.Timeout
import com.enragedginger.notkea.actors.Cashier.CheckoutReq
import com.enragedginger.notkea.actors.Customer.{ShoppingList, GoShopping, GetShoppingList}
import com.enragedginger.notkea.actors.FloorWorker.{FetchItemSuccess, FetchItem}
import com.enragedginger.notkea.actors.StoreManager.HandleComplaint
import com.enragedginger.notkea.akkaguice.NamedActor
import com.enragedginger.notkea.models.Item
import com.enragedginger.notkea.services.ShoppingListGenerator
import com.google.inject.name.Named
import scala.concurrent.Future
import scala.concurrent.duration._
import akka.pattern.ask

/**
 * Represents a customer visiting a Notkea store.
 * @author Stephen Hopper.
 */
class Customer @Inject()(shoppingListGenerator: ShoppingListGenerator,
                          @Named("cashier") cashier: ActorRef,
                          @Named("floorworker") floorWorker: ActorRef,
                          @Named("storemanager") storeManager: ActorRef) extends BaseNotkeaActor {

  import scala.concurrent.ExecutionContext.Implicits.global
  implicit val timeout: Timeout = 120 seconds

  private lazy val shoppingList: ShoppingList = shoppingListGenerator.generateShoppingList()

  override def receive = {
    case GetShoppingList =>
      //log.info(s"Received request for shopping list.")
      sender() ! shoppingList
    case GoShopping =>
      //log.info("Customer is going shopping!")
      val requester = sender()

      val shoppingF = shoppingList.map {
        case (quantity, item) => floorWorker ? FetchItem(item = item, quantity = quantity)
      }

      for {
        cartAttempts <- Future.sequence(shoppingF)
      } yield {
        val fetchedItems = cartAttempts.filter(_.isInstanceOf[FetchItemSuccess]).map(_.asInstanceOf[FetchItemSuccess]).map {
          fetchedItem => (fetchedItem.quantity, fetchedItem.req.item)
        }
        val totalRequestedUnits = shoppingList.map(_._1).sum
        val totalReceivedUnits = fetchedItems.map(_._1).sum
        //log.info(s"Successfully fetched ${fetchedItems.size} of ${shoppingList.size} different items and $totalReceivedUnits units of $totalRequestedUnits requested units.")

        if (totalRequestedUnits > 0 && totalReceivedUnits == 0) {
          storeManager ! HandleComplaint(s"I came here to pick up $totalRequestedUnits units and you had $totalReceivedUnits of them! I'm angry!")
        } else {
          cashier ! CheckoutReq(fetchedItems)
        }

        requester ! "done"
      }
  }
}

object Customer extends NamedActor {
  override final val name = "customer"

  case object GetShoppingList
  case object GoShopping
  type ShoppingList = Seq[(Int, Item)]
}