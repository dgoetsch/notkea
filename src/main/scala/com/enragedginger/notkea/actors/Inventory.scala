package com.enragedginger.notkea.actors

import javax.inject.{Singleton, Inject}

import com.enragedginger.notkea.actors.Inventory.{AddItems, RemoveItems, LookupItem, GetInventory}
import com.enragedginger.notkea.akkaguice.NamedActor
import com.enragedginger.notkea.models.InventoryEntry
import com.enragedginger.notkea.repositories.InventoryRepository

/**
 * Represents the inventory at a single Notkea store.
 * @author Stephen Hopper.
 */
@Singleton
class Inventory @Inject()(inventoryRepository: InventoryRepository) extends BaseNotkeaActor {

  lazy val inventory = inventoryRepository.readInventoryFromFile()

  override def receive: Receive = {
    case GetInventory => {
      //log.info(s"Processing request to get inventory.")
      sender() ! getInventory
    }
    case LookupItem(id) => {
      //log.info(s"Looking up item with id $id.")
      sender() ! lookupItem(id)
    }
    case RemoveItems(id, quantity) => {
      //log.info(s"Removing $quantity units of item with id $id.")
      sender() ! removeItems(id, quantity)
    }
    case AddItems(id, quantity) => {
      //log.info(s"Adding $quantity units of item with id $id.")
      sender() ! addItems(id, quantity)
    }
  }

  private def addItems(id: Long, quantity: Int): Unit = {
    inventory.get(id) match {
      case Some(entry) =>
        val newQuantity = entry.quantity + quantity
        inventory.update(id, entry.copy(quantity = newQuantity))
      case _ => {}
    }
  }

  private def getInventory: Seq[InventoryEntry] = {
    inventory.map {
      case (id, entry) => entry
    }.toSeq
  }

  private def lookupItem(id: Long): Option[InventoryEntry] = {
    inventory.get(id)
  }

  private def removeItems(id: Long, quantity: Int): Int = {
    inventory.get(id) match {
      case Some(entry) if entry.quantity >= quantity =>
        val newQuantity = entry.quantity - quantity
        inventory.update(id, entry.copy(quantity = newQuantity))
        quantity
      case Some(entry) if entry.quantity > 0 =>
        val newQuantity = entry.quantity - quantity
        inventory.update(id, entry.copy(quantity = newQuantity))
        entry.quantity
      case _ => 0
    }
  }
}

object Inventory extends NamedActor {
  override final val name = "inventory"

  case object GetInventory
  case class LookupItem(id: Long)
  case class RemoveItems(id: Long, quantity: Int)
  case class AddItems(id: Long, quantity: Int)
}
