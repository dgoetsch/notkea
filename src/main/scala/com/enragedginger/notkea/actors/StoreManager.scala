package com.enragedginger.notkea.actors

import javax.inject.{Singleton, Inject}

import com.enragedginger.notkea.actors.StoreManager.{GetSalesReport, GetComplaints, HandleComplaint, RecordSale, SalesReport, InventoryPullResult}
import com.enragedginger.notkea.akkaguice.NamedActor

import scala.collection.mutable.ListBuffer

/**
 * Represents a store manager at a Notkea store.
 * @author Stephen Hopper.
 */
@Singleton
class StoreManager @Inject()() extends BaseNotkeaActor {

  private var complaints = List[String]()
  private var sales = List[RecordSale]()
  private val inventoryPullResults = ListBuffer[InventoryPullResult]()

  override def receive: Receive = {
    case HandleComplaint(message) =>
      //log.info(s"Received complaint $message")
      //log the complaint for later
      complaints = message :: complaints
    case GetComplaints => sender() ! complaints
    case sale: RecordSale => sales = sale :: sales
    case GetSalesReport =>
      val missedUnits = inventoryPullResults.map(res => res.requestedQuantity - res.pulledQuantity).sum
      val missedRevenue = inventoryPullResults.map(res => (res.requestedQuantity - res.pulledQuantity) * res.pricePerUnit).sum
      val totalUnitsSold = sales.map(_.units).sum
      val totalRevenue = sales.map(_.price).sum
      sender() ! SalesReport(
        totalUnitsSold = totalUnitsSold,
        totalRevenue = totalRevenue,
        missedUnits = missedUnits,
        missedRevenue = missedRevenue,
        unitEfficiency = totalUnitsSold.toFloat / (totalUnitsSold + missedUnits),
        salesEfficiency = totalRevenue / (totalRevenue + missedRevenue)
      )
    case res: InventoryPullResult => inventoryPullResults += res
  }
}

object StoreManager extends NamedActor {
  override final val name = "storemanager"

  case class HandleComplaint(message: String)
  case object GetComplaints
  case class RecordSale(units: Int, price: Float)
  case object GetSalesReport
  case class SalesReport(totalUnitsSold: Int, totalRevenue: Float, missedUnits: Int, missedRevenue: Float,
                         unitEfficiency: Float, salesEfficiency: Float)
  case class InventoryPullResult(requestedQuantity: Int, pulledQuantity: Int, pricePerUnit: Float)
}