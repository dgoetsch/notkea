package com.enragedginger.notkea.actors

import javax.inject.Inject

import akka.pattern.ask
import akka.actor.ActorRef
import akka.util.Timeout
import com.enragedginger.notkea.actors.FloorWorker.{FetchItemSuccess, FetchItemFailure, RestockItem, FetchItem}
import com.enragedginger.notkea.actors.Inventory.{AddItems, RemoveItems}
import com.enragedginger.notkea.actors.StoreManager.InventoryPullResult
import com.enragedginger.notkea.akkaguice.NamedActor
import com.enragedginger.notkea.models.{Item, InventoryEntry}
import com.google.inject.name.Named
import scala.concurrent.duration._
import scala.util.{Failure, Success}

/**
 * Represents a floor worker in a Notkea store. This worker is in charge of fetching items from the inventory.
 * @author Stephen Hopper.
 */
class FloorWorker @Inject()(@Named("inventory") inventory: ActorRef,
                            @Named("storemanager") storeManager: ActorRef) extends BaseNotkeaActor {

  import scala.concurrent.ExecutionContext.Implicits.global
  implicit val timeout: Timeout = 120 seconds

  override def receive = {
    case req: FetchItem =>
      //log.info(s"Attempting to fetch ${req.quantity} units of ${req.item.name}")
      val customer = sender()
      (inventory ? RemoveItems(req.item.id, req.quantity)).onComplete {
        case Success(withdrawnQuantity: Int) if withdrawnQuantity > 0 =>
          storeManager ! InventoryPullResult(requestedQuantity = req.quantity, pulledQuantity = withdrawnQuantity, pricePerUnit = req.item.price)
          customer ! FetchItemSuccess(req, withdrawnQuantity)
        case Success(withdrawnQuantity: Int) if withdrawnQuantity == 0 =>
          storeManager ! InventoryPullResult(requestedQuantity = req.quantity, pulledQuantity = withdrawnQuantity, pricePerUnit = req.item.price)
          customer ! FetchItemFailure(req = req, message = s"We were unable to fetch any units of ${req.item.name}")
        case Failure(failure) =>
          log.error(failure, s"An error occurred while fetching ${req.quantity} instances of item ${req.item.id}")
          customer ! FetchItemFailure(req = req, message = failure.getMessage)
      }
    case RestockItem(id, quantity) =>
      //TODO: Add some error handling around this
      inventory ! AddItems(id, quantity)
  }
}

object FloorWorker extends NamedActor {
  override final val name = "floorworker"

  case class FetchItem(item: Item, quantity: Int)
  case class FetchItemSuccess(req: FetchItem, quantity: Int)
  case class FetchItemFailure(req: FetchItem, message: String)
  case class RestockItem(id: Long, quantity: Int)
}