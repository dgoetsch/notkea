package com.enragedginger.notkea.actors

import akka.actor.{Actor, ActorLogging}

/**
 * Base actor for Notkea.
 * @author Stephen Hopper.
 */
trait BaseNotkeaActor extends Actor with ActorLogging {

}
