package com.enragedginger.notkea.modules

import com.enragedginger.notkea.services.ShoppingListGenerator
import com.google.inject.AbstractModule

/**
 * @author Stephen Hopper.
 */
class ServiceModule extends AbstractModule {
  override def configure(): Unit = {
    bind(classOf[ShoppingListGenerator])
  }
}
