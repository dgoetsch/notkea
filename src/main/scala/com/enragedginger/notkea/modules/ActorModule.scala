package com.enragedginger.notkea.modules

import javax.inject.Singleton

import akka.actor.{Actor, ActorRef, ActorSystem}
import akka.routing.{BalancingPool, RoundRobinPool}
import com.enragedginger.notkea.actors._
import com.enragedginger.notkea.akkaguice.GuiceAkkaActorRefProvider
import com.google.inject.name.{Named, Names}
import com.google.inject.{Inject, name, Provides, AbstractModule}

/**
 * Module for wiring actors for DI.
 * @author Stephen Hopper.
 */
class ActorModule extends AbstractModule with GuiceAkkaActorRefProvider {
  override def configure(): Unit = {
    bindActor(Cashier.name, classOf[Cashier])
    bindActor(Customer.name, classOf[Customer])
    bindActor(FloorWorker.name, classOf[FloorWorker])
    bindActor(StoreManager.name, classOf[StoreManager])
    bindActor(Inventory.name, classOf[Inventory])
  }

  @Provides
  @Singleton
  @Named("cashier")
  def provideCashierRef(@Inject() system: ActorSystem): ActorRef = system.actorOf(BalancingPool(25).props(propsFor(system, Cashier.name)), Cashier.name)

  @Provides
  @Named("customer")
  def provideCustomerRef(@Inject() system: ActorSystem): ActorRef = provideActorRef(system, Customer.name)

  @Provides
  @Singleton
  @Named("floorworker")
  def provideFloorWorkerRef(@Inject() system: ActorSystem): ActorRef = system.actorOf(BalancingPool(50).props(propsFor(system, FloorWorker.name)), FloorWorker.name)

  @Provides
  @Singleton
  @Named("storemanager")
  def provideStoreManagerRef(@Inject() system: ActorSystem): ActorRef = system.actorOf(propsFor(system, StoreManager.name), StoreManager.name)

  @Provides
  @Singleton
  @Named("inventory")
  def provideInventoryRef(@Inject() system: ActorSystem): ActorRef = system.actorOf(propsFor(system, Inventory.name), Inventory.name)

  private def bindActor[T <: Actor](name: String, actorClass: Class[T]): Unit = {
    bind(classOf[Actor]).annotatedWith(Names.named(name)).to(actorClass)
  }
}
