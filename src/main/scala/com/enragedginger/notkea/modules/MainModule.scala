package com.enragedginger.notkea.modules

import com.enragedginger.notkea.akkaguice.AkkaModule
import com.google.inject.AbstractModule

/**
 * Main module for wiring other modules for dependency injection.
 * @author Stephen Hopper.
 */
class MainModule extends AbstractModule {
  override def configure(): Unit = {
    install(new AkkaModule())
    install(new ActorModule())
    install(new ConfigModule())
    install(new RepositoryModule())
    install(new ServiceModule())
  }
}
