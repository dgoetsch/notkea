package com.enragedginger.notkea.services

import javax.inject.{Inject, Singleton}

import com.enragedginger.notkea.models.Item
import com.enragedginger.notkea.repositories.InventoryRepository

import scala.util.Random

/**
 * Service for generating shopping lists for a customer.
 * @author Stephen Hopper.
 */
@Singleton
class ShoppingListGenerator @Inject()(inventoryRepository: InventoryRepository) {

  private lazy val items = inventoryRepository.readInventoryFromFile().values.toList
  private lazy val randy = new Random(System.currentTimeMillis())
  private val MaxShoppingListSize = 5
  private val MaxUnitSize = 3

  /**
   * Generates a list of tuples consisting of item counts and item instances. Lists are generated randomly from the
   * inventory.
   * @return A random shopping list based on the base inventory.
   */
  def generateShoppingList(): Seq[(Int, Item)] = {
    val potentialShoppingListSizes = List.range(1, items.size + 1)
    val shoppingListSize = Math.min(randy.nextInt(potentialShoppingListSizes.size) + 1, MaxShoppingListSize)

    scala.util.Random.shuffle(items.combinations(shoppingListSize).toList).head.map {
      entry =>
        val requestedQuantity = Math.min(randy.nextInt(entry.quantity) + 1, MaxUnitSize)
        (requestedQuantity, entry.item)
    }
  }
}
