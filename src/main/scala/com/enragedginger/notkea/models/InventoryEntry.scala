package com.enragedginger.notkea.models

/**
 * Model for associating an item with a quantity.
 * @author Stephen Hopper.
 */
case class InventoryEntry(quantity: Int, item: Item)
