package com.enragedginger.notkea.models

/**
 * Represents an item in the store's catalogue.
 * @author Stephen Hopper.
 */
case class Item(id: Long, name: String, description: String, price: Float)
