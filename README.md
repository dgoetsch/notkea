# NotKEA - Store Simulator #

NotKEA is a Scala + Akka application which simulates a retail store. Customers arrive at the store with their shopping
lists, request that the floor workers retrieve the items on the list on their behalf, and then checkout at a register
to complete their shopping experience. Everything is completed using asynchronous message passing.

## Building ##
Build the application with sbt using `sbt compile`

## Running ##
Run the simulation with sbt and specify the number of customers as a command line argument. For example,
the following command will run the simulation with 80 customers and output to a file called face.out for further review:
`sbt "run 80" > face.out`